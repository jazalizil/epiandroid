package epitech.epiandroid.epitechapi;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

/**
 * Created by jazalizil on 13/01/15.
 */
public final class Client {
    private static String token;
    private static HttpClient httpclient = new DefaultHttpClient();
    public static final String POST_REQUEST = "postRequest";
    public static final String GET_REQUEST = "getRequest";

    public static String getToken() {
        return token;
    }

    private static void setToken(String token) {
        Client.token = token;
    }

    public static JSONObject getRequest(String url, List<NameValuePair> args) throws IOException, JSONException {
        String paramString = URLEncodedUtils.format(args, "utf-8");

        url += "?" + paramString;
        HttpGet httpget = new HttpGet(url);
        HttpResponse response = httpclient.execute(httpget);
        String responseBody = EntityUtils.toString(response.getEntity());
        return new JSONObject(responseBody);
    }

    public static JSONObject postRequest(String url, List<NameValuePair> postArgs) throws IOException, JSONException {
        HttpPost httppost = new HttpPost(url);
        httppost.setEntity(new UrlEncodedFormEntity(postArgs));
        HttpResponse response = httpclient.execute(httppost);
        String responseBody = EntityUtils.toString(response.getEntity());
        return new JSONObject(responseBody);
    }
}
