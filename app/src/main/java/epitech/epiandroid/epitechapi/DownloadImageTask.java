package epitech.epiandroid.epitechapi;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.widget.ImageView;

import java.io.InputStream;

import epitech.epiandroid.R;
import epitech.epiandroid.activities.MainActivity;
import epitech.epiandroid.beans.UserInfos;
import epitech.epiandroid.fragments.EpiProgressDialogFragment;

/**
 * Created by jazalizil on 18/01/15.
 */
public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
    FragmentActivity mActivity;
    EpiProgressDialogFragment progressDialog;

    public DownloadImageTask(FragmentActivity activity) {
        this.mActivity = activity;
        progressDialog = EpiProgressDialogFragment.newInstance();
    }

    @Override
    protected void onPreExecute() {
        progressDialog.show(mActivity.getFragmentManager(), mActivity.getString(R.string.load_img));
    }
    @Override
    protected Bitmap doInBackground(String... params) {
        Bitmap bmImg = null;
        String url = params[0];
        try {
            InputStream in = new java.net.URL(url).openStream();
            bmImg = BitmapFactory.decodeStream(in);
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bmImg;
    }
    @Override
    protected void onPostExecute(final Bitmap result) {
        progressDialog.dismiss();
    }

    @Override
    protected void onCancelled()
    {
        progressDialog.dismiss();
    }
}
