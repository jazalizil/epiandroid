package epitech.epiandroid.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import epitech.epiandroid.R;
import epitech.epiandroid.beans.EpiUrl;
import epitech.epiandroid.beans.UserInfos;
import epitech.epiandroid.epitechapi.DownloadImageTask;
import epitech.epiandroid.utils.EpiAndroidUtils;

/**
 * Created by jazalizil on 26/01/15.
 */
public class MainFragment extends EpiFragment {

    private static final String IMG_RESPONSE = "manageImageResponse";

    public MainFragment() {
        super();
        mRequest.url = new EpiUrl(mPostMethod, R.string.infos_action);
        layoutId = R.layout.fragment_main;
    }

    public void manageImageResponse() {
        try {
            DownloadImageTask dwnldImg = new DownloadImageTask(mActivity);
            dwnldImg.execute(mRequest.response.getString(EpiAndroidUtils.URL));
            mActivity.mUser.userImageBmp = dwnldImg.get();
            mActivity.mNavigationDrawerFragment.setUser(mActivity.mUser);
            //mActivity.updateView(R.id.imageViewMainFragment, ImageView.class.getDeclaredMethod("setImageBitmap", Bitmap.class), mActivity.mUser.userImageBmp);
            ((ImageView)mView.findViewById(R.id.imageViewMainFragment)).setImageBitmap(mActivity.mUser.userImageBmp);
            //((ImageView) mActivity.findViewById(R.id.imageViewMainFragment)).setImageBitmap(mActivity.mUser.userImageBmp);
        } catch (JSONException | InterruptedException | ExecutionException /*| NoSuchMethodException*/ e) {
            e.printStackTrace();
        }
    }

    @Override
    public void manageResponse() {
        Method setTextMeth = null;
        ArrayList<NameValuePair> imgDownloadArgs = new ArrayList<>();
        final String INFOS = "infos";
        JSONObject infos;

        mRequest.url.requestMethod = mGetMethod;
        mRequest.url.urlId = R.string.photo_action;
        mRequest.parameters.add(new BasicNameValuePair(EpiAndroidUtils.LOGIN, mActivity.mLogin));

        try {
            new RequestApiTask(getClass().getDeclaredMethod(IMG_RESPONSE)).execute();
            setTextMeth = TextView.class.getDeclaredMethod("setText", CharSequence.class);
            infos = mRequest.response.getJSONObject(INFOS);
            mActivity.mUser = new UserInfos(infos);
            mActivity.updateView(R.id.mTitle, setTextMeth, mActivity.mUser.title);
            mActivity.updateView(R.id.mDesc, setTextMeth, mActivity.mUser.login + " - Promo " + Integer.toString(mActivity.mUser.promo));
            //text.setText(infos.toString(4));
        } catch (JSONException | NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup vG, Bundle savedInstance) {
        super.onCreateView(inflater, vG, savedInstance);
        new RequestApiTask().execute();
        return mView;
    }

}
