package epitech.epiandroid.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.apache.http.message.BasicNameValuePair;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import epitech.epiandroid.R;
import epitech.epiandroid.beans.EpiUrl;

/**
 * Created by jazalizil on 26/01/15.
 */
public class PlaningFragment extends EpiFragment {

    public PlaningFragment() {
        super();
        mRequest.url = new EpiUrl(mPostMethod, R.string.planning_action);
        layoutId = R.layout.fragment_planing;
    }

    public void manageResponse() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup vG, Bundle savedInstance) {
        super.onCreateView(inflater, vG, savedInstance);
        SimpleDateFormat sdf = new SimpleDateFormat(getString(R.string.date_complete_format));
        GregorianCalendar gC = new GregorianCalendar();

        mRequest.parameters.add(new BasicNameValuePair(getString(R.string.planing_start), sdf.format(gC.getTime())));
        gC.roll(GregorianCalendar.MONTH, true);
        mRequest.parameters.add(new BasicNameValuePair(getString(R.string.planing_end), sdf.format(gC.getTime())));
        new RequestApiTask().execute();
        return mView;
    }
}
