package epitech.epiandroid.fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class EpiAlertDialogFragment extends DialogFragment {

    private static final String TITLE = "title";
    private static final String CONTENT = "content";
    private static final String BUTTON_OK = "button_ok";
    private static final String BUTTON_KO = "button_ko";
    private DialogInterface.OnClickListener buttonOkListener;
    private DialogInterface.OnClickListener buttonKoListener;

    public static EpiAlertDialogFragment newInstance(int title, int button_ok, int button_ko, String content) {
        EpiAlertDialogFragment frag = new EpiAlertDialogFragment();
        Bundle args = new Bundle();
        args.putInt(TITLE, title);
        args.putString(CONTENT, content);
        args.putInt(BUTTON_OK, button_ok);
        args.putInt(BUTTON_KO, button_ko);
        frag.setArguments(args);
        return frag;
    }

    public void setButtonOkListener(DialogInterface.OnClickListener buttonOkListener) {
        this.buttonOkListener = buttonOkListener;
    }

    public void setButtonKoListener(DialogInterface.OnClickListener buttonKoListener) {
        this.buttonKoListener = buttonKoListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int title = getArguments().getInt(TITLE);
        String content = getArguments().getString(CONTENT);
        int button_ok = getArguments().getInt(BUTTON_OK);
        int button_ko = getArguments().getInt(BUTTON_KO);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(content)
                .setPositiveButton(button_ok, buttonOkListener);

        if (buttonKoListener != null)
            builder.setNegativeButton(button_ko, buttonKoListener);

        return builder.create();
    }
}
