package epitech.epiandroid.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import epitech.epiandroid.R;
import epitech.epiandroid.beans.EpiUrl;

/**
 * Created by jazalizil on 26/01/15.
 */
public class MarksFragment extends EpiFragment {
    public MarksFragment() {
        super();
        mRequest.url = new EpiUrl(mPostMethod, R.string.infos_action);
        layoutId = R.layout.fragment_main;
    }

    public void manageResponse() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup vG, Bundle savedInstance) {
        super.onCreateView(inflater, vG, savedInstance);
        new RequestApiTask().execute();
        return mView;
    }

}
