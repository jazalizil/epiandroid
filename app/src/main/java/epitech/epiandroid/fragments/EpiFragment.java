package epitech.epiandroid.fragments;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.concurrent.ExecutionException;

import epitech.epiandroid.R;
import epitech.epiandroid.activities.MainActivity;
import epitech.epiandroid.beans.Request;
import epitech.epiandroid.epitechapi.Client;
import epitech.epiandroid.utils.EpiAndroidUtils;

/**
 * Created by jazalizil on 26/01/15.
 */
public abstract class EpiFragment extends Fragment {
    protected RequestApiTask mRequestTask;
    protected Request mRequest;
    protected MainActivity mActivity;
    protected final String RESPONSE_MNG = "manageResponse";
    protected Method mPostMethod;
    protected Method mGetMethod;
    protected int layoutId;
    protected View mView;

    public EpiFragment() {
        mRequest = new Request();

        try {
            mPostMethod = Client.class.getDeclaredMethod(Client.POST_REQUEST, String.class, List.class);
            mGetMethod = Client.class.getDeclaredMethod(Client.GET_REQUEST, String.class, List.class);

        } catch (NoSuchMethodException e) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(layoutId, container, false);
        mView = v;
        return v;
    }

    @Override
    public void onAttach(Activity act) {
        super.onAttach(act);
        mActivity = (MainActivity) act;
        mRequest.parameters.add(new BasicNameValuePair(
                EpiAndroidUtils.TOKEN,
                mActivity.getIntent().getStringExtra(EpiAndroidUtils.TOKEN)));
    }

    protected abstract void manageResponse();


    public class RequestApiTask extends AsyncTask<Void, Void, Request> {

        public FragmentActivity mActivity;
        private Method callOnPostExec;
        private EpiProgressDialogFragment mProgressDialog;

        public RequestApiTask() {
            mProgressDialog = EpiProgressDialogFragment.newInstance();
            try {
                this.callOnPostExec = EpiFragment.this.getClass().getDeclaredMethod(RESPONSE_MNG);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
        public RequestApiTask(Method callOnPostExec) {
            mProgressDialog = EpiProgressDialogFragment.newInstance();
            this.callOnPostExec = callOnPostExec;
        }

        @Override
        protected void onPreExecute() {
            mProgressDialog.show(getActivity().getFragmentManager(), getString(R.string.loading_sentence));
        }
        @Override
        protected Request doInBackground(Void... params) {
            Request current = mRequest;
            JSONObject response;

            try {
                response = (JSONObject)current.url.requestMethod.invoke(this, getString(current.url.urlId), current.parameters);

                if (response.keys().next().equals(EpiAndroidUtils.ERROR)) {
                    current.errorStr = response.getJSONObject(EpiAndroidUtils.ERROR).getString(EpiAndroidUtils.MESSAGE);
                }
                current.response = response;
            } catch (JSONException | IllegalAccessException | InvocationTargetException e) {
                current.errorStr = e.getMessage();
            }
            return current;
        }

        @Override
        protected void onPostExecute(final Request result) {
            EpiAlertDialogFragment dialogFrag;

            mProgressDialog.dismiss();
            if (result.errorStr != null) {
                dialogFrag = EpiAlertDialogFragment.newInstance(R.string.connection_failure_title, R.string.button_ok, R.string.button_ko, mRequest.errorStr);
                dialogFrag.show(getActivity().getSupportFragmentManager(), getString(R.string.connection_failure_label));
            }
            else {
                mRequest = result;
                try {
                    callOnPostExec.invoke(EpiFragment.this);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        protected void onCancelled() {
            mProgressDialog.dismiss();
        }
    }

}
