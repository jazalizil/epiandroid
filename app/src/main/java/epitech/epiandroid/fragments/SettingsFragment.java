package epitech.epiandroid.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import epitech.epiandroid.R;

/**
 * Created by jazalizil on 26/01/15.
 */
public class SettingsFragment extends EpiFragment {
    public SettingsFragment() {
        layoutId = R.layout.fragment_settings;
    }

    @Override
    protected void manageResponse() {
        
    }
}
