package epitech.epiandroid.beans;

/**
 * Created by jazalizil on 21/01/15.
 */
public class IntentArg {
    String content;
    String key;

    public IntentArg(String key, String content) {
        this.key = key;
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public String getKey() {
        return key;
    }
}
