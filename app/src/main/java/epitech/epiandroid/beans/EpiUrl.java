package epitech.epiandroid.beans;

import java.lang.reflect.Method;

/**
 * Created by jazalizil on 19/01/15.
 */
public class EpiUrl {
    public Method requestMethod;
    public int urlId;

    public EpiUrl(Method requestMethod, int urlId) {
        this.requestMethod = requestMethod;
        this.urlId = urlId;
    }

}
