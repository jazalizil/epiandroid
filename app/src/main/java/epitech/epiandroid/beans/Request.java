package epitech.epiandroid.beans;

import org.apache.http.NameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by jazalizil on 17/01/15.
 */
public class Request {
    public EpiUrl url;
    public ArrayList<NameValuePair> parameters;
    public JSONObject response;
    public String errorStr;

    public Request() {
        parameters = new ArrayList<>();
    }

    public Request(EpiUrl url) {
        this.url = url;
    }
}
