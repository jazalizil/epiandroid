package epitech.epiandroid.beans;


import android.graphics.Bitmap;

/**
 * Created by jazalizil on 21/01/15.
 */
public class NavigationViewModel {
    private Bitmap userImage;
    private String title;
    private int promo;
    private boolean isGroupHeader = false;

    public NavigationViewModel(String title) {
        super();
        this.title = title;
    }

    public NavigationViewModel() {
        this(null);
        this.isGroupHeader = true;
    }

    public NavigationViewModel(Bitmap userImage, String title, int promo) {
        this(title);
        this.userImage = userImage;
        this.promo = promo;
        this.isGroupHeader = true;
    }

    public Bitmap getUserImage() {
        return userImage;
    }

    public void setUserImage(Bitmap userImage) {
        this.userImage = userImage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPromo() {
        return promo;
    }

    public void setPromo(int promo) {
        this.promo = promo;
    }

    public boolean isGroupHeader() {
        return isGroupHeader;
    }

    public void setGroupHeader(boolean isGroupHeader) {
        this.isGroupHeader = isGroupHeader;
    }
}
