package epitech.epiandroid.beans;

import android.graphics.Bitmap;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.Iterator;

/**
 * Created by jazalizil on 13/01/15.
 */
public class UserInfos {
    public String title;
    public String firstName;
    public String lastName;
    public String internal_email;
    public String login;
    public String id;
    public String picture;
    public int promo;
    public int uid;
    public int gid;
    public String location;
    public Bitmap userImageBmp;

    public UserInfos(JSONObject infos) {
        Field f;
        String key;
        Iterator<String> keys;

        keys = infos.keys();
        while (keys.hasNext()) {
            key = keys.next();
            try {
                f = UserInfos.class.getDeclaredField(key);
                f.setAccessible(true);
                f.set(this, infos.get(key));
            } catch (NoSuchFieldException | JSONException | IllegalAccessException e) {
            }
        }
    }
}
