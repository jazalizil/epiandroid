package epitech.epiandroid.utils;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import epitech.epiandroid.R;
import epitech.epiandroid.beans.NavigationViewModel;

/**
 * Created by jazalizil on 21/01/15.
 */
public class EpiListAdapter extends ArrayAdapter<NavigationViewModel> {
    private Context con;
    private ArrayList<NavigationViewModel> modelsArrayList;

    public EpiListAdapter(Context con, ArrayList<NavigationViewModel> models) {
        super(con, android.R.layout.simple_list_item_activated_2, models);
        this.con = con;
        this.modelsArrayList = models;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // 1. Create inflater
        LayoutInflater inflater = (LayoutInflater) con
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // 2. Get rowView from inflater

        View rowView = null;
        if (modelsArrayList.get(position).isGroupHeader()) {
            rowView = inflater.inflate(R.layout.snippet_title, parent, false);

            // 3. Get icon,title & counter views from the rowView
            TextView nameTextView = (TextView) rowView.findViewById(R.id.nameText);
            //TextView promoTextView = (TextView) rowView.findViewById(R.id.promoText);
            ImageView image = (ImageView) rowView.findViewById(R.id.userImage);

            // 4. Set the text for textView and the user image
            image.setImageBitmap(modelsArrayList.get(position).getUserImage());
            nameTextView.setText(modelsArrayList.get(position).getTitle());
            //promoTextView.setText(Integer.toString(modelsArrayList.get(position).getPromo()));
        } else {
            rowView = inflater.inflate(android.R.layout.simple_list_item_activated_1, parent, false);
            TextView text = (TextView) rowView.findViewById(android.R.id.text1);
            text.setText(modelsArrayList.get(position).getTitle());

        }

        // 5. retrn rowView
        return rowView;
    }

    public void modelChanged(ArrayList<NavigationViewModel> models) {
        this.modelsArrayList = models;
        this.notifyDataSetChanged();
    }

}
