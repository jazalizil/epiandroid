package epitech.epiandroid.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;

import epitech.epiandroid.R;
import epitech.epiandroid.beans.IntentArg;
import epitech.epiandroid.fragments.EpiAlertDialogFragment;

/**
 * Created by jazalizil on 14/01/15.
 */
public final class EpiAndroidUtils {
    public static final String ERROR = "error";
    public static final String MESSAGE = "message";
    public static final String TOKEN = "token";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String URL = "url";
    public static final int NO_SECTION = 0;
    public static final String CACHE = "cache";

    public static boolean isNetworkAvailable(Context con) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) con.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void enableNetworking(final FragmentActivity act) {
        EpiAlertDialogFragment dialogFrag;

        dialogFrag = EpiAlertDialogFragment.newInstance(R.string.connection_failure_title, R.string.button_ok, R.string.button_ko, "connection");
        dialogFrag.setButtonOkListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                act.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
            }
        });
        dialogFrag.show(act.getSupportFragmentManager(), act.getResources().getString(R.string.login_failure_label));
    }

    public static void switchActivity(Activity current, Class cls) {
        current.finish();

        Intent intent = new Intent(current, cls);
        current.startActivity(intent);


    }

    public static void switchActivity(Activity current, Class cls, IntentArg... args) {
        current.finish();

        Intent intent = new Intent(current, cls);
        for (IntentArg a : args) {
            if (a != null)
                intent.putExtra(a.getKey(), a.getContent());
        }
        current.startActivity(intent);

    }
}
