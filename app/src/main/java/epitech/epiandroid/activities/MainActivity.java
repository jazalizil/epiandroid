package epitech.epiandroid.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.lang.reflect.Method;

import epitech.epiandroid.R;
import epitech.epiandroid.beans.UserInfos;
import epitech.epiandroid.fragments.CreditsFragment;
import epitech.epiandroid.fragments.EpiAlertDialogFragment;
import epitech.epiandroid.fragments.EpiFragment;
import epitech.epiandroid.fragments.MainFragment;
import epitech.epiandroid.fragments.MarksFragment;
import epitech.epiandroid.fragments.NavigationDrawerFragment;
import epitech.epiandroid.fragments.PlaningFragment;
import epitech.epiandroid.fragments.ProjectsFragment;
import epitech.epiandroid.fragments.SettingsFragment;
import epitech.epiandroid.fragments.TrombiFragment;
import epitech.epiandroid.utils.EpiAndroidUtils;

public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    public static String mLogin;
    public Menu mOptMenu;
    public UserInfos mUser;

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */

    public NavigationDrawerFragment mNavigationDrawerFragment;


    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    private String mCache;

    private static Class[] mFragments = new Class[]{
            MainFragment.class,
            PlaningFragment.class,
            MarksFragment.class,
            ProjectsFragment.class,
            TrombiFragment.class,
            SettingsFragment.class,
            CreditsFragment.class
    };

    public void updateView(int viewId, Method updateMethod, Object... args) {
        /*View elem =findViewById(viewId);
        Log.e("updateView", "llala");
        try {
            updateMethod.invoke(elem, args);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }*/
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLogin = getIntent().getStringExtra(EpiAndroidUtils.LOGIN);
        EpiAlertDialogFragment dialogFrag;

        mCache = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE).getString(EpiAndroidUtils.CACHE, null);
        // if no cache and no connection, please active it!
        if (!EpiAndroidUtils.isNetworkAvailable(this) && mCache == null) {
            EpiAndroidUtils.enableNetworking(this);
        }

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    protected void onStop() {
        super.onStop();

        // We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        SharedPreferences settings = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(EpiAndroidUtils.CACHE, mCache);

        // Commit the edits!
        editor.apply();
    }



    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        EpiFragment frag = null;
        try {
            frag = (EpiFragment) mFragments[position].newInstance();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, frag)
                    .commit();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }


    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        final View container = findViewById(R.id.container);
        final View progressView = findViewById(R.id.load_progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            container.setVisibility(show ? View.GONE : View.VISIBLE);
            container.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    container.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            container.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    void disconnect() {
        SharedPreferences prefs = getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(EpiAndroidUtils.LOGIN);
        editor.remove(EpiAndroidUtils.TOKEN);
        editor.commit();
        EpiAndroidUtils.switchActivity(this, LoginActivity.class);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            this.mOptMenu = menu;
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_settings:
                Toast.makeText(this, "Setting action.", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_disconnect:
                disconnect();
                return true;
            case R.id.menuRefresh:
                Toast.makeText(this, "Refresh", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}