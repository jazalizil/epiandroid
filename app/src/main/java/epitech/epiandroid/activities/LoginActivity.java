package epitech.epiandroid.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Context;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import epitech.epiandroid.R;
import epitech.epiandroid.beans.IntentArg;
import epitech.epiandroid.epitechapi.Client;
import epitech.epiandroid.utils.EpiAndroidUtils;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends FragmentActivity implements LoaderCallbacks<Cursor> {

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    /*private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };*/
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;


    // UI references.
    private AutoCompleteTextView mLoginView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private SharedPreferences sharedprefs;
    private String savedLogin;
    private String savedPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Setting up
        Button mEmailSignInButton = (Button) findViewById(R.id.sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        mLoginView = (AutoCompleteTextView) findViewById(R.id.login);
        mPasswordView = (EditText) findViewById(R.id.password);

        // Check if user is already logged in
        sharedprefs = getSharedPreferences(getResources().getString(R.string.preferences), Context.MODE_PRIVATE);
        savedLogin = sharedprefs.getString(EpiAndroidUtils.LOGIN, null);
        savedPassword = sharedprefs.getString(EpiAndroidUtils.PASSWORD, null);
        if (savedLogin != null && savedPassword != null) {
            mLoginView.setText(savedLogin);
            mPasswordView.setText(savedPassword);
            executeLoginTask(savedLogin, savedPassword);
        }

        // Set up the login form.
        populateAutoComplete();

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });


    }

    private void populateAutoComplete() {
        getLoaderManager().initLoader(0, null, this);
    }

    private void executeLoginTask(String login, String password) {
        showProgress(true);
        mAuthTask = new UserLoginTask(login, password);
        mAuthTask.execute((Void) null);
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mLoginView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String login = mLoginView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;


        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid login.
        if (TextUtils.isEmpty(login)) {
            mLoginView.setError(getString(R.string.error_field_required));
            focusView = mLoginView;
            cancel = true;
        } else if (!isLoginValid(login)) {
            mLoginView.setError(getString(R.string.error_invalid_login));
            focusView = mLoginView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            executeLoginTask(login, password);
        }
    }

    private boolean isLoginValid(String login) {
        return login.contains("_") && login.length() == 8;
    }

    private boolean isPasswordValid(String password) {
        return password.length() == 8;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {


        private final List<NameValuePair> args;
        private String errorMessage;
        private String token;

        UserLoginTask(String login, String password) {
            args = new ArrayList<>(2);
            args.add(new BasicNameValuePair(EpiAndroidUtils.LOGIN, login));
            args.add(new BasicNameValuePair(EpiAndroidUtils.PASSWORD, password));
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            JSONObject data;
            String key;

            try {
                data = Client.postRequest(getResources().getString(R.string.login_action), args);
                key = data.keys().next();
                if (key.equals(EpiAndroidUtils.ERROR)) {
                    errorMessage = data.getJSONObject(EpiAndroidUtils.ERROR).getString(EpiAndroidUtils.MESSAGE);
                    return false;
                } else if (savedLogin == null) {
                    SharedPreferences.Editor editor = sharedprefs.edit();
                    editor.putString(EpiAndroidUtils.LOGIN, args.get(0).getValue());
                    editor.putString(EpiAndroidUtils.PASSWORD, args.get(1).getValue());
                    editor.apply();
                }
                token = data.getString(EpiAndroidUtils.TOKEN);
                return true;
            } catch (IOException | JSONException e) {
                errorMessage = e.getMessage();
            }
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success || savedLogin != null) {
                EpiAndroidUtils.switchActivity(
                        LoginActivity.this,
                        MainActivity.class,
                        new IntentArg(EpiAndroidUtils.TOKEN, token),
                        new IntentArg(EpiAndroidUtils.LOGIN, args.get(0).getValue()));
            } else if (!EpiAndroidUtils.isNetworkAvailable(LoginActivity.this)) {
                EpiAndroidUtils.enableNetworking(LoginActivity.this);
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}